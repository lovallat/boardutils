# BoardUtils

> A small compilation of some of the tools that I used and coded to help me 
> debug a school project consisting of a board game.

- [BoardUtils](#boardutils)
  - [Board Delimiter](#board-delimiter)
    - [Exit Codes](#exit-codes)

## Board Delimiter

### Exit Codes

- ``0`` everything is all good
- ``1`` wrong usage