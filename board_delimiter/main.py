import sys
import getopt
from PIL import Image, ImageDraw, ImageFont, ImageGrab


def usage():
    print("python3 main.py -i|--input <input file|'clipboard'> -o|--output <output file> -b|--board <board size (int)>")


def drawLinesOnImage(inputFile, outputFile, boardSize):
    lineColor = 255
    offsetPaste = 40
    font = ImageFont.truetype("res/fonts/open_sans.ttf", 30)
    if inputFile == "clipboard":
        im = ImageGrab.grabclipboard()
    else:
        im = Image.open(inputFile)
    width, height = im.size
    div_w = width / boardSize
    div_h = height / boardSize
    out = Image.new("RGB", (width + offsetPaste, height +
                            offsetPaste), (255, 255, 255))
    out.paste(im, (offsetPaste, offsetPaste))

    draw = ImageDraw.Draw(out)
    for i in range(boardSize):
        draw.line((div_w * i + offsetPaste, 0, div_w * i +
                   offsetPaste, height + offsetPaste), fill=lineColor)
        draw.line((0, div_h * i + offsetPaste, width + offsetPaste,
                   div_h * i + offsetPaste), fill=lineColor)
        draw.text((0 + offsetPaste / 2, div_h * i + offsetPaste * 1.65),
                  text=str(i), fill='black', font=font, anchor="mm")
        draw.text((div_w * i + offsetPaste * 1.35, 0 + offsetPaste / 2),
                  text=str(i), fill='black', font=font, anchor="mm")

    out.save(outputFile)


def main(argv):
    boardSize = 0
    inputFile = ""
    outputFile = ""
    try:
        opts, args = getopt.getopt(
            argv, "hb:i:o:", ["board=", "input=", "output="])
    except getopt.GetoptError:
        usage()
        exit(1)
    for opt, arg in opts:
        if opt == '-h':
            usage()
            exit(0)
        elif opt in ("-b", "--board"):
            boardSize = arg
        elif opt in ("-i", "--input"):
            inputFile = arg
        elif opt in ("-o", "--output"):
            outputFile = arg
    if int(boardSize) <= 0 or len(inputFile) == 0 or len(outputFile) == 0:
        usage()
        exit(1)
    else:
        drawLinesOnImage(inputFile, outputFile, int(boardSize))


if __name__ == "__main__":
    main(sys.argv[1:])
